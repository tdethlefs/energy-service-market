from application import config
from helper.LocationHelper import check_local_condition
import derssystems.derFactory

class Registry:
    """ Registry Instance. Defined as Singleton! Thus use getInstance ONLY! """
    __instance = None

    # ===================================
    # ===== Singleton Konstruktion  =====
    # ===================================
    @staticmethod
    def getInstance():
        if Registry.__instance is None:
            Registry()
        return Registry.__instance

    def __init__(self, config=None):
        if Registry.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            Registry.__instance = self

            self.derSystems = dict()
            if config is None:
                self.__generate_default_set()

    # ============================================================
    # ===== Generierung eines default sets für die Registry  =====
    # ============================================================
    def __generate_default_set(self):
        """
        Erzeugt ein default Set an Anlagen.
        :param num_ders: Die Gesamtanzahl der Anlagen.
        :param der_distr: Die exliziten Anlagenszahlen
        """

        for i in range(config.num_gas):
            d = derssystems.derFactory.create_stochastic_conventional(name='DER_' + str(i),
                                                                      strategie=config.globale_strategie,
                                                                      locational=config.local)
            d.erzeuge_tagesprofil(0)
            self.derSystems[d.name] = d

        for i in range(config.num_bio):
            d = derssystems.derFactory.create_stochastic_biogas(name='DER_BIO_' + str(i),
                                                                strategie=config.globale_strategie,
                                                                locational=config.local)
            d.erzeuge_tagesprofil(0)
            self.derSystems[d.name] = d

        for i in range(config.num_wka):
            d = derssystems.derFactory.create_stochastic_wind(name='DER_WKA_' + str(i),
                                                              strategie=config.globale_strategie,
                                                              locational=config.local)
            d.erzeuge_tagesprofil(0)
            self.derSystems[d.name] = d

        for i in range(config.num_sol):
            d = derssystems.derFactory.create_stochastic_solar(name='DER_SOL_' + str(i),
                                                               strategie=config.globale_strategie,
                                                               locational=config.local)
            d.erzeuge_tagesprofil(0)
            self.derSystems[d.name] = d



    # ======================================================
    # ===== Gibt alle Angebote in der Registry zurück  =====
    # ======================================================
    def get_angebote(self, localize=None):
        offers = dict()
        for key, der in self.derSystems.items():
            if not der.saturated and check_local_condition(der, localize):
                offer = der.erzeuge_offer()
                offers[der.name] = offer
        return offers

    # ========================================================
    # ===== Buchung einer Anlage durch einen Aggregator  =====
    # ========================================================
    def angebot_buchen(self, aggregator, der, fahrplan, bezahlung):
        dersystem = self.derSystems[der]
        if dersystem is None: return False
        return dersystem.buche_anlage(aggregator, fahrplan, bezahlung)

    # ==============================================================
    # ===== Erzegunung neuer Tagesprofile für die DER Systeme  =====
    # ==============================================================
    def erzeuge_neue_tagesprofile(self, day):
        for key, der in self.derSystems.items():
            der.erzeuge_tagesprofil(day)

    # =========================================================
    # ===== Export der DER Systeme als Dict. für reg_dump =====
    # =========================================================
    def export(self, day):
        portfolio = dict()
        for key in self.derSystems:
            der = self.derSystems[key]
            portfolio[key] = der.export(day)

        return portfolio


