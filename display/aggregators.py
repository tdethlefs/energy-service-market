import plotly
from plotly import tools
import plotly.graph_objs as go
from helper.DateHelper import create_day_timerow


# ================================================
# ===== 1-Tages Portfolio eines Aggregators  =====
# ================================================
def show_aggregator_portfolio(aggregator, direct_plot=True, filename='portfolio-plot.html'):
    data = []
    for key in aggregator['portfolio']:
        der = aggregator['portfolio'][key]

        trace0 = go.Scatter(
            x=create_day_timerow(180),
            y=der['fahrplan'],
            name=key,
            mode = 'lines',
            stackgroup='one'
        )

        data.append(trace0)

    trace1 = go.Scatter(
        x=create_day_timerow(180),
        y=aggregator['tagesprofil'],
        name='Tagesprofil',
        stackgroup='profil'
    )
    data.append(trace1)

    layout = dict(title='Portfolio des Aggregators',
                  xaxis=dict(title='Viertelstunde des Tages'),
                  yaxis=dict(title='Energie in MWh'),
                  )

    fig = dict(data=data, layout=layout)
    if direct_plot is True:
        plotly.offline.plot(fig, auto_open=True, output_type='file', filename=filename)
    else:
        return fig
