from display import helper, aggregators
from helper.DateHelper import create_day_timerow
import application.config as config
import display.config as dc
from operator import add, truediv
import plotly
from plotly import tools
import plotly.graph_objs as go
import numpy as np

# =========================================================
# ===== 1-Tagesprofile der Aggregatoren übereinander  =====
# =========================================================
def show_aggregators_one_day(aggregators_dump, direct_plot=True):
    fig = tools.make_subplots(rows=len(aggregators_dump), cols=1)
    i = 1
    for aggregator in aggregators_dump:
        agg_fig = aggregators.show_aggregator_portfolio(aggregator, direct_plot=False)
        for trace in agg_fig['data']:
            fig.append_trace(trace, i, 1)
        i += 1

    fig['layout'].update(height=5000, title='Aggregator Portfolios')
    plotly.offline.plot(fig, auto_open=direct_plot, output_type='file', filename='aggregator-plot.html')


# ========================================================
# ===== Indexpreis ggü. Utilization von DER Anlagen  =====
# ========================================================
def index_vs_utilization(reg_dump, direct_plot=True):
    x_axis = []
    y_axis = []

    for key, der in reg_dump.items():
        der_sum = der['aktivierungspreis'] + der['arbeitspreis'] + der['bereitstellungspreis']
        util = der['telemetry']['utilization']
        x_axis.append(der_sum)
        y_axis.append(util)

    trace = go.Scatter(
        x=x_axis,
        y=y_axis,
        mode='markers'
    )

    data = [trace]
    layout = dict(title='util vs index',
                  xaxis=dict(title='Index in €/MWh'),
                  yaxis=dict(title='Utilization'),
                  )
    fig = dict(data=data, layout=layout)
    plotly.offline.plot(fig, auto_open=direct_plot, output_type='file', filename='registry-plot-util-index.html')


# =========================================================
# === Nutzungsgrad der Registry Kapazität über den Tag  ===
# =========================================================
def registry_intraday_utilization(reg_dump, direct_plot=True):
    average_util = [0.0 for i in range(96)]
    data = []
    buttons = []
    visibility = [False for i in range(len(reg_dump) + 1)]
    visibility[0] = True

    for day in reg_dump:
        # Sum up all schedules for each day
        gesamtpotential = [0.0 for i in range(96)]
        gesamtfahrplan = [0.0 for i in range(96)]
        for der_key, der in reg_dump[day].items():
            gesamtpotential = list(map(add, gesamtpotential, der['potential']))
            gesamtfahrplan = list(map(add,gesamtfahrplan,der['gesamtfahrplan']))

        # Berechne day_util und füge die Werte dem AVG hinzu
        day_util = list(map(truediv, gesamtfahrplan, gesamtpotential))
        average_util = list(map(add, day_util, average_util))

        # Erzeuge einen trace für die Darstellung
        trace = go.Scatter( x=create_day_timerow(0), y=day_util,
                            name='Utilization Tag ' + str(day), visible=False)

        data.append(trace)
        new_visible = visibility.copy()
        new_visible[int(day) + 1] = True
        buttons.append(dict(
                label='Utilization Tag ' + str(day), method='update',
                args=[{'visible': new_visible}, {'title': 'Utilization Tag ' + str(day)}]))

    average_util[:] = [x / len(reg_dump) for x in average_util]
    trace_avg = go.Scatter(
            x=create_day_timerow(0), y=average_util,
            name='Avg Utilization', visible=True
    )
    data.insert(0, trace_avg)

    buttons.insert(0, dict(
                label='Utilization AVG', method='update',
                args=[{'visible': visibility}, {'title': 'Utilization Average'}]))

    updatemenus = list([dict(active=0, buttons=buttons,)])

    layout = dict(title='Util der Registry',
                  xaxis=dict(title='Viertelstunde des Tages'),
                  yaxis=dict(title='Utilization of Registry'),
                  updatemenus=updatemenus
                  )

    fig = dict(data=data, layout=layout)
    plotly.offline.plot(fig, auto_open=direct_plot, output_type='file', filename='registry-util.html')


# =========================================================
# ===== Profitabilität über Index Preise der Anlagen  =====
# =========================================================
def eprofatibilitaet(reg_dump, filename='', direct_plot=True):
    """ Diese Show-Methode generiet eine Grafik, die die Indexpresie übder der Profitabilität anzeigt. Die e-Profitabilität
    der Anlagen ist definiert als Gewinn in € / Angebotene Energie in MWh, die Einheit ist somit €/MWh.
    :param reg_dump: 1-Tages Dump der Registry
    """
    axis_sol = dict(x_axis=[], y_axis=[])
    axis_wka = dict(x_axis=[], y_axis=[])
    axis_bio = dict(x_axis=[], y_axis=[])
    axis_gas = dict(x_axis=[], y_axis=[])

    for key, der in reg_dump.items():
        der_index = der['aktivierungspreis'] + der['arbeitspreis'] + der['bereitstellungspreis']
        profitabilitaet = der['telemetry']['eprofitabilitaet']
        if profitabilitaet > 200:
            print('PROFIT!')
            print(der)
        if der['typ'] is 'WKA': axis = axis_wka
        if der['typ'] is 'PV': axis = axis_sol
        if der['typ'] is 'Bio': axis = axis_bio
        if der['typ'] is 'Gas': axis = axis_gas

        axis['x_axis'].append(der_index)
        axis['y_axis'].append(profitabilitaet)

    if len(filename) == 0 or filename is '':
        filename='registry-profitabilitaet.html'

    helper.der_class_plot(title='E-Profitabilität der DER Systeme',
                          xtitle='Index in €/MWh',
                          ytitle='Profitabilität in €/MWh(Gewinn/Angebotene Leistung)',
                          filename=filename,
                          axis_sol=axis_sol,
                          axis_wka=axis_wka,
                          axis_bio=axis_bio,
                          axis_gas=axis_gas,
                          direct_plot=direct_plot)


# ===========================================================
# ===== Signaturplot - Anlagengrößen über Indexpreisen  =====
# ===========================================================
def signaturplot_nominal_index(reg_dump, filename='', direct_plot=True):
    """ Diese Methode bekommt als Parameter den Registry Dump eines Tages und zeigt die Signatur der DER Systeme an
    (Größe i.S.v. Leistung und Indexpreise). Damit kann validiert werden, dass die Struktur
    realistisch / ausgewogen ist.
    :param reg_dump: 1-Tages Dump der Registry
    """
    axis_sol = dict(x_axis=[], y_axis=[])
    axis_wka = dict(x_axis=[], y_axis=[])
    axis_bio = dict(x_axis=[], y_axis=[])
    axis_gas = dict(x_axis=[], y_axis=[])

    for key, der in reg_dump.items():
        index_preis = der['aktivierungspreis'] + der['arbeitspreis'] + der['bereitstellungspreis']
        nom_leistung = der['nominalleistung']
        if der['typ'] is 'WKA': axis = axis_wka
        if der['typ'] is 'PV': axis = axis_sol
        if der['typ'] is 'Bio': axis = axis_bio
        if der['typ'] is 'Gas': axis = axis_gas

        axis['y_axis'].append(index_preis)
        axis['x_axis'].append(nom_leistung)

    if len(filename) == 0 or filename is '':
        filename='registry-struktur.html'

    layout = dict(title='Struktur der Registry (Indexpreis über Leistung)', xaxis=dict(title='Nominalleistung in MW'), yaxis=dict(title='Index in €/MWh'))

    helper.der_class_plot(title='Struktur der Registry (Indexpreis über Leistung)',
                          ytitle='Index in €/MWh',
                          xtitle='Nominalleistung',
                          layout=layout,
                          filename=filename,
                          axis_sol=axis_sol,
                          axis_wka=axis_wka,
                          axis_bio=axis_bio,
                          axis_gas=axis_gas,
                          direct_plot=direct_plot)


# =========================================================
# =====  Util über Profitabilität der Anlagen  =====
# =========================================================
def eprofatibilitaet_util(reg_dump, filename='', direct_plot=True):
    axis_sol = dict(x_axis=[], y_axis=[])
    axis_wka = dict(x_axis=[], y_axis=[])
    axis_bio = dict(x_axis=[], y_axis=[])
    axis_gas = dict(x_axis=[], y_axis=[])
    for key, der in reg_dump.items():
        util = der['telemetry']['utilization']
        profitabilitaet = der['telemetry']['eprofitabilitaet']
        if der['typ'] is 'WKA': axis = axis_wka
        if der['typ'] is 'PV': axis = axis_sol
        if der['typ'] is 'Bio': axis = axis_bio
        if der['typ'] is 'Gas': axis = axis_gas

        axis['x_axis'].append(profitabilitaet)
        axis['y_axis'].append(util)

    if len(filename) == 0 or filename is '':
        filename='registry-profitabilitaet.html'

    helper.der_class_plot(title='E-Profitabilität der DER Systeme vs Util',
                          ytitle='Util',
                          xtitle='Profitabilität in €/MWh(Gewinn/Angebotene Leistung)',
                          filename=filename,
                          axis_sol=axis_sol,
                          axis_wka=axis_wka,
                          axis_bio=axis_bio,
                          axis_gas=axis_gas,
                          direct_plot=direct_plot)


# =========================================================
# =====  Util über Index der Anlagen  =====
# =========================================================
def index_util(reg_dump, filename='', direct_plot=True):
    axis_sol = dict(x_axis=[], y_axis=[])
    axis_wka = dict(x_axis=[], y_axis=[])
    axis_bio = dict(x_axis=[], y_axis=[])
    axis_gas = dict(x_axis=[], y_axis=[])
    for key, der in reg_dump.items():
        util = der['telemetry']['utilization']
        der_index = der['arbeitspreis'] + der['bereitstellungspreis'] + der['aktivierungspreis']
        ftyp = der['typ']
        if der['typ'] == 'WKA':
            axis = axis_wka
        if der['typ'] == 'PV':
            axis = axis_sol
        if der['typ'] == 'Bio':
            axis = axis_bio
        if der['typ'] == 'Gas':
            axis = axis_gas

        axis['y_axis'].append(der_index)
        axis['x_axis'].append(util)

    if len(filename) == 0 or filename is '':
        filename='registry-profitabilitaet.html'

    helper.der_class_plot(title='Index der DER Systeme und Nutzungsgrad',
                          xtitle='cf',
                          ytitle='Indexpreis in €/MWh',
                          filename=filename,
                          axis_sol=axis_sol,
                          axis_wka=axis_wka,
                          axis_bio=axis_bio,
                          axis_gas=axis_gas,
                          direct_plot=direct_plot)


# =========================================================
# =====  Anteil der Anlagen  =====
# =========================================================
def anteil_anlagen_registry(reg_dump, filename='', relative_plot=False, direct_plot=True):
    # Die Anzahl der Tage
    days = len(reg_dump)

    wka_potential = [0.0 for i in range(days)]
    sol_potential = [0.0 for i in range(days)]
    bio_potential = [0.0 for i in range(days)]
    gas_potential = [0.0 for i in range(days)]
    total_potential = [0.0 for i in range(days)]

    for day in reg_dump:
        iday = int(day)
        for der_key, der in reg_dump[day].items():
            potential_sum = sum(der['potential'])
            total_potential[iday] += potential_sum
            if der['typ'] is 'WKA': wka_potential[iday] += potential_sum
            if der['typ'] is 'PV': sol_potential[iday] += potential_sum
            if der['typ'] is 'Bio': bio_potential[iday] += potential_sum
            if der['typ'] is 'Gas': gas_potential[iday] += potential_sum

    # Relative assignments
    if relative_plot:
        wka_potential = list(map(truediv, wka_potential, total_potential))
        sol_potential = list(map(truediv, sol_potential, total_potential))
        bio_potential = list(map(truediv, bio_potential, total_potential))
        gas_potential = list(map(truediv, gas_potential, total_potential))

    line_max = [max(wka_potential), max(sol_potential), max(bio_potential), max(gas_potential)]
    y_limit = max(line_max) * 1.1

    x_axis = [i for i in range(days)]

    trace_wka = go.Scatter(x=x_axis, y=wka_potential, name='WKA Potential', line=dict(color=dc.color_wka))
    trace_sol = go.Scatter(x=x_axis, y=sol_potential, name='PV Potential', line=dict(color=dc.color_sol))
    trace_bio = go.Scatter(x=x_axis, y=bio_potential, name='Bio Potential', line=dict(color=dc.color_bio))
    trace_gas = go.Scatter(x=x_axis, y=gas_potential, name='Gas Potential', line=dict(color=dc.color_gas))

    data = [trace_wka, trace_sol, trace_bio, trace_gas]
    layout = dict(title='Potential der Energieträger',
                  xaxis=dict(title='Tage',
                             # tickmode='linear',
                             # tick0=0,
                             # dtick=1.0,
                             ),
                  yaxis=dict(title='Energie in MWh',
                             range=[0, y_limit]
                             ),
                  )
    fig = dict(data=data, layout=layout)
    if filename is '':
        filename = 'registry-potentiale-der-traeger.html'
    plotly.offline.plot(fig, auto_open=direct_plot, output_type='file', filename=filename)


def registry_anteil(reg_dump, aggregator_dump, filename='', relative_plot=False, direct_plot=True):

    # Die Anzahl der Tage
    days = len(reg_dump)

    # Der Energiebedarf eines Tages (Summe aller Aggregator-Tagesprofile)
    energiebedarf = [0.0 for i in range(days)]

    # Der Anteil der Registry an dem Energiebedarf (Summe der DER-Profile in der Registry)
    registry_share = [0.0 for i in range(days)]         # Summe der DER Profile

    # Summe des Potentials in der Registry (Summe der einzelnen DER Potentiale über den Tag)
    registry_potential = [0.0 for i in range(days)]

    for day in aggregator_dump:
        iday = int(day)
        for aggregator in aggregator_dump[day]:
            energiebedarf[iday] += sum(aggregator['tagesprofil'])

    for day in reg_dump:
        iday = int(day)
        for der_key, der in reg_dump[day].items():
            registry_potential[iday] += sum(der['potential'])
            registry_share[iday] += sum(der['gesamtfahrplan'])

    # Relative assignments
    if relative_plot:
        registry_potential = list(map(truediv, registry_potential, energiebedarf))
        registry_share = list(map(truediv, registry_share, energiebedarf))
        energiebedarf = list(map(truediv, energiebedarf, energiebedarf))

    line_max = [max(registry_potential), max(registry_share), max(energiebedarf)]
    y_limit = max(line_max) * 1.1

    x_axis = [i for i in range(days)]

    trace_share = go.Scatter(
        x=x_axis,
        y=registry_share,
        name='Anteil der Registry'
    )

    trace_potential = go.Scatter(
        x=x_axis,
        y=registry_potential,
        name='Potential der Registry'
    )

    trace_demand = go.Scatter(
        x=x_axis,
        y=energiebedarf,
        name='Energiebedarf'
    )

    data = [trace_share, trace_potential, trace_demand]
    layout = dict(title='Anteil der Registry (WIP)',
                  xaxis=dict(title='Tage',
                             # tickmode='linear',
                             # tick0=0,
                             # dtick=1.0,
                             ),
                  yaxis=dict(title='Energie in MWh',
                             range=[0, y_limit]
                             ),
                  )
    fig = dict(data=data, layout=layout)
    if filename is '':
        filename = 'registry-anteil.html'
    plotly.offline.plot(fig, auto_open=direct_plot, output_type='file', filename=filename)


def registry_index_entwicklung(reg_dump, filename='', direct_plot=True):

    # Die Anzahl der Tage
    days = len(reg_dump)

    avg_index = [0.0 for i in range(days)]
    wka_index = [0.0 for i in range(days)]
    sol_index = [0.0 for i in range(days)]
    bio_index = [0.0 for i in range(days)]
    gas_index = [0.0 for i in range(days)]

    for day in reg_dump:
        iday = int(day)
        for key, der in reg_dump[day].items():
            der_index = der['aktivierungspreis'] + der['arbeitspreis'] + der['bereitstellungspreis']
            avg_index[iday] += der_index
            if der['typ'] is 'WKA': wka_index[iday] += der_index
            if der['typ'] is 'PV': sol_index[iday] += der_index
            if der['typ'] is 'Gas': gas_index[iday] += der_index
            if der['typ'] is 'Bio': bio_index[iday] += der_index

    total_ders = config.num_gas + config.num_bio + config.num_wka + config.num_sol
    avg_index[:] = [x / total_ders for x in avg_index]
    wka_index[:] = [x / config.num_wka for x in wka_index]
    sol_index[:] = [x / config.num_sol for x in sol_index]
    bio_index[:] = [x / config.num_bio for x in bio_index]
    gas_index[:] = [x / config.num_gas for x in gas_index]

    line_max = [max(avg_index), max(wka_index), max(sol_index), max(gas_index), max(bio_index)]
    y_limit = max(line_max) * 1.1
    x_axis = [i for i in range(days)]

    trace_avg = go.Scatter(x=x_axis, y=avg_index, name='Durchschnittlicher Index', line=dict(color='rgb(0, 0, 0)'))
    trace_wka = go.Scatter(x=x_axis, y=wka_index, name='Wind Index', line=dict(color=dc.color_wka))
    trace_sol = go.Scatter(x=x_axis, y=sol_index, name='Solar Index', line=dict(color=dc.color_sol))
    trace_bio = go.Scatter(x=x_axis, y=bio_index, name='Bio Index', line=dict(color=dc.color_bio))
    trace_gas = go.Scatter(x=x_axis, y=gas_index, name='Gas Index', line=dict(color=dc.color_gas))

    data = [trace_avg, trace_wka, trace_sol, trace_gas, trace_bio]
    layout = dict(title='Entwicklung der Indexpreisgebote',
                  xaxis=dict(title='Tage',
                             # tickmode='linear',
                             # tick0=0,
                             # dtick=1.0,
                             ),
                  yaxis=dict(title='Index in €/MWh',
                             range=[0, y_limit]
                             ),
                  )
    fig = dict(data=data, layout=layout)
    if filename is '':
        filename = 'registry-index-entwicklung.html'
    plotly.offline.plot(fig, auto_open=direct_plot, output_type='file', filename=filename)


def profitabilitatsentwicklung(reg_dump, filename='', direct_plot=True):
    days = len(reg_dump)
    avg_eprof = [0.0 for i in range(days)]
    avg_util = [0.0 for i in range(days)]

    for day in reg_dump:
        iday = int(day)
        for key, der in reg_dump[day].items():
            avg_eprof[iday] += der['telemetry']['eprofitabilitaet']
            avg_util[iday] += der['telemetry']['utilization']

    total_ders = config.num_gas + config.num_bio + config.num_wka + config.num_sol
    avg_eprof[:] = [x / total_ders for x in avg_eprof]
    avg_util[:] = [x / total_ders for x in avg_util]

    np_avg = np.array(avg_eprof)
    np_util = np.array(avg_util)
    print("Mittelwert Profit pro angebotene MWh: " + str(np.mean(np_avg)))
    print("Std. Abw. Profit pro angebotene MWh: " + str(np.std(np_avg)))

    rev_per_sold_mwh = np.multiply(np_avg, 1 / np_util)
    print("Mittelwert Profit pro verkaufte MWh: " + str(np.mean(rev_per_sold_mwh)))
    print("Std. Abw. Profit pro verkaufte MWh: " + str(np.std(rev_per_sold_mwh)))


    y_limit = max(avg_eprof) * 1.1
    x_axis = [i for i in range(days)]
    trace = go.Scatter(x=x_axis, y=avg_eprof)

    data = [trace]
    layout = dict(title='Entwicklung des durchschnittlichen Umsatz',
                  xaxis=dict(title='Tage',
                             # tickmode='linear',
                             # tick0=0,
                             # dtick=1.0,
                             ),
                  yaxis=dict(title='Umsatz pro angebotener MWh in €/MWh',
                             range=[0, y_limit]
                             ),
                  )
    fig = dict(data=data, layout=layout)
    if filename is '':
        filename = 'registry-profit-entwicklung.html'
    plotly.offline.plot(fig, auto_open=direct_plot, output_type='file', filename=filename)


def registry_index_entwicklung_local(reg_dump, filename='', direct_plot=True, location=1):

    # Die Anzahl der Tage
    days = len(reg_dump)

    avg_index = [0.0 for i in range(days)]
    wka_index = [0.0 for i in range(days)]
    sol_index = [0.0 for i in range(days)]
    bio_index = [0.0 for i in range(days)]
    gas_index = [0.0 for i in range(days)]

    cnt_wka = len({k: v for k, v in reg_dump[0].items() if v['typ'] is 'WKA' and v['location'] == int(location)})
    cnt_sol = len({k: v for k, v in reg_dump[0].items() if v['typ'] is 'PV' and v['location'] == int(location)})
    cnt_bio = len({k: v for k, v in reg_dump[0].items() if v['typ'] is 'Bio' and v['location'] == int(location)})
    cnt_gas = len({k: v for k, v in reg_dump[0].items() if v['typ'] is 'Gas' and v['location'] == int(location)})

    for day in reg_dump:
        iday = int(day)
        for key, der in reg_dump[day].items():
            if der['location'] == int(location):
                der_index = der['aktivierungspreis'] + der['arbeitspreis'] + der['bereitstellungspreis']
                avg_index[iday] += der_index
                if der['typ'] is 'WKA': wka_index[iday] += der_index
                if der['typ'] is 'PV': sol_index[iday] += der_index
                if der['typ'] is 'Gas': gas_index[iday] += der_index
                if der['typ'] is 'Bio': bio_index[iday] += der_index
            else:
                pass

    total_ders = cnt_wka + cnt_bio + cnt_sol + cnt_gas
    avg_index[:] = [x / total_ders for x in avg_index]
    if cnt_wka > 0: wka_index[:] = [x / cnt_wka for x in wka_index]
    if cnt_sol > 0: sol_index[:] = [x / cnt_sol for x in sol_index]
    if cnt_bio > 0: bio_index[:] = [x / cnt_bio for x in bio_index]
    if cnt_gas > 0: gas_index[:] = [x / cnt_gas for x in gas_index]

    line_max = [max(avg_index), max(wka_index), max(sol_index), max(gas_index), max(bio_index)]
    y_limit = max(line_max) * 1.1
    x_axis = [i for i in range(days)]

    trace_avg = go.Scatter(x=x_axis, y=avg_index, name='Durchschnittlicher Index', line=dict(color='rgb(0, 0, 0)'))
    trace_wka = go.Scatter(x=x_axis, y=wka_index, name='Wind Index', line=dict(color=dc.color_wka))
    trace_sol = go.Scatter(x=x_axis, y=sol_index, name='Solar Index', line=dict(color=dc.color_sol))
    trace_bio = go.Scatter(x=x_axis, y=bio_index, name='Bio Index', line=dict(color=dc.color_bio))
    trace_gas = go.Scatter(x=x_axis, y=gas_index, name='Gas Index', line=dict(color=dc.color_gas))

    data = [trace_avg, trace_wka, trace_sol, trace_gas, trace_bio]
    layout = dict(title='Entwicklung der Indexpreisgebote Location ' + str(location),
                  xaxis=dict(title='Tage',
                             # tickmode='linear',
                             # tick0=0,
                             # dtick=1.0,
                             ),
                  yaxis=dict(title='Index in €/MWh',
                             range=[0, y_limit]
                             ),
                  )
    fig = dict(data=data, layout=layout)
    if filename is '':
        filename = 'registry-index-entwicklung-location-' + str(location) + '.html'
    plotly.offline.plot(fig, auto_open=direct_plot, output_type='file', filename=filename)
