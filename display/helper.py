import plotly
import plotly.graph_objs as go
import display.config as dc


def der_class_plot(title='', xtitle='', ytitle='', layout=None, filename='def-plot.html',
                   axis_sol=dict(), axis_wka=dict(), axis_gas=dict(), axis_bio=dict(), direct_plot=True):

    trace_sol = go.Scatter(x=axis_sol['x_axis'], y=axis_sol['y_axis'], mode='markers',
                           name='Solar', marker=dict(color=dc.color_sol))

    trace_wka = go.Scatter(x=axis_wka['x_axis'], y=axis_wka['y_axis'], mode='markers',
                           name='WKA', marker=dict(color=dc.color_wka))

    trace_gas = go.Scatter(x=axis_gas['x_axis'], y=axis_gas['y_axis'], mode='markers',
                           name='Gas', marker=dict(color=dc.color_gas))

    trace_bio = go.Scatter(x=axis_bio['x_axis'], y=axis_bio['y_axis'], mode='markers',
                           name='Bio', marker=dict(color=dc.color_bio))

    data = [trace_sol, trace_wka, trace_gas, trace_bio]

    line_y_max = [max(axis_sol['y_axis']), max(axis_wka['y_axis']), max(axis_gas['y_axis']), max(axis_bio['y_axis'])]
    y_limit = max(line_y_max) * 1.1
    line_x_max = [max(axis_sol['x_axis']), max(axis_wka['x_axis']), max(axis_gas['x_axis']), max(axis_bio['x_axis'])]
    x_limit = max(line_x_max) * 1.1

    if layout is None:
        layout = dict(title=title, xaxis=dict(title=xtitle, range=[0, x_limit]), yaxis=dict(title=ytitle, range=[0, y_limit]),)

    fig = dict(data=data, layout=layout)
    plotly.offline.plot(fig, auto_open=direct_plot, output_type='file', filename=filename)