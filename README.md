# Energy Service Market (ESM)
----
The Energy Service Market (ESM) is a game-theoretic simulation of an energy market for energy services, matching demand and supply curves. The ESM is developed in Python and can be used for long-term investigations of service-driven energy markets. This README covers the following topics:

  - [Installation](#Installation)
  - [Required Libraries](#Required-Libraries-and-Versions)
  - [Configuration](#Configuration)
  - [License](#License)
  - [Contact information](#Contact-information)

## Installation

ESM requires [Python 3.7](https://python.org)  to run. Please install it according to the documentation   [here](https://docs.python.org/3.7/).

### Setup
First download and extract the code from GitLab on your local system. Navigate via shell to the folder of the software. Then, install the dependencies from the requirements.txt by using pip and run the application.

```sh
$ pip install -r requirements.txt
$ python run application\App.py
```

Or alternatevely, you can use the IDE of your choice, e.g. [PyCharm](https://www.jetbrains.com/de-de/pycharm/). 

### Run Simulations
The ESM simulator can be started by 

```sh
$ python run application/App.py
```
Results will be stored in seprated folders within the application/run file. Please ensure that the application has the rights to write to that folder. You can identify a result set by the folder with the according date and time information together with a eight element ASCII-Fingerprint. A folder for a result-collection will look like this example:

```sh
$ application/runs/20-07-14-11-00-19-25e39b8b
```

Results will contain the config.json, the most important key-performance-indicators (kennzahlen.json und kennzahlen-uebersicht.json), and some graphical output as HTML-files. You can add detailed information for each DER System and Aggregtor for each day by adding these two lines to the end in the [application/app.py](application/app.py):

```sh
write_dump_to_json(reg_dump, filepath + 'reg_dump.json')
write_dump_to_json(aggregators_dump, filepath + 'agg_dump.json')
```

**CAUTION! These files will be multiple Gigabyte in size even for small simulations!**

## Required Libraries and Versions

The following libraries and packages have been used in the development of the ESM. Please ensure that you use the exact same versions as this software is not maintained and shipped as it without warrenty.

| Plugin | Version | PyPI-Link |
| ------ | ------ | ------ |
| Certifi | 2018.11.29 | [Docs](https://pypi.org/project/certifi/) |
| chardet | 3.0.4 | [Docs](https://pypi.org/project/chardet/) |
| decorator | 4.3.2 | [Docs](https://pypi.org/project/decorator/) |
| idna | 2.8 | [Docs](https://pypi.org/project/idna/)|
| ipython-genutils | 0.2.0 | [Docs](https://pypi.org/project/ipython_genutils/) |
| jsonschema | 2.6.0 |[Docs](https://pypi.org/project/jsonschema/) |
| jupyter-core | 4.4.0 | [Docs](https://pypi.org/project/jupyter-core/) |
| nbformat | 4.4.0 | [Docs](https://pypi.org/project/nbformat/) |
| numpy | 1.16.1 | [Docs](https://pypi.org/project/numpy/) |
| pip | 10.0.1 | [Docs](https://pypi.org/project/pip/) |
| plotly | 3.6.1 | [Docs](https://pypi.org/project/plotly/) |
| pytz | 2018.9 | [Docs](https://pypi.org/project/pytz/) |
| requests | 2.21.0 | [Docs](https://pypi.org/project/requests/) |
| retrying | 1.3.3 | [Docs](https://pypi.org/project/retrying/) |
| setuptools | 39.1.0 |[Docs](https://pypi.org/project/setuptools/) |
| six | 1.12.0 | [Docs](https://pypi.org/project/six/) |
| trailets | 4.3.2 | [Docs](https://pypi.org/project/trailets/) |
| urllib3 | 1.24.1 | [Docs](https://pypi.org/project/urllib3/) |

## Configuration

The main configuration for the simulation can be found in [application/config.json](application/config.json). It is based on data from the German Government and the US-EIA. The parameters of the simulation are explained within the smaple configuration file shipped with this software.


The software uses the H0 Standard Load Profile of Households issued by the BDEW (Bund Deutscher Energie- und Wasserwirtschaft). You can find the description [here](https://www.bdew.de/energie/standardlastprofile-strom/). The profile is stored in [application/slph0.csv](application/slph0.csv).

# License
----
Copyright 2020 Tim Dethlefs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

# Contact Information
----
For questions, remarks or bufg reports, please contact me via mail: [Tim Dethlefs](tim.dethlefs@haw-hamburg.de).