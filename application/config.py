"""
====================================================
========== IRES SIM SAMPLE CONFIG V.3.0.0 ==========
====================================================

- Gas Power plant class = Advanced CC
- Pricing Sigma-Spread 20%
- Power plant sizes according to the Kraftwerkliste and the EEG-Anlagenregister und PV-Meldeportal
- Corrected Pricing, including the levelized capital costs
- Pricing according to the U.S. EIA Report, for solar and wind the variable costs are the transmission costs!

Date of issue: 14.07.2020

Tim Dethlefs
"""

# ===== GLOBAL CONFIG =====
version = "3.0.0"
num_tage = 15

# ==== Locational Parameters ====
# The locational parameters. num_locations determines the number of local areas to be considered. Everything else is
# considered global. Each loaction has a share that is equal, so the number of global Aggregators and DER Systems is
# determined by 1-(num_loactions * share) = global_share.
# local_exclusive determines the exclusivity of the areas i.e. global aggregators can access the resources in that
# particular area.
# local_permanency determines the persistancy of the locations. If 1.0, they'll be always online and demanding.
# 0.0 means always OFF. Everything inbetween determines the likelihood of the location to occur.
local = False                # turns localization on and off
local_num_locations = 1     # Number of local entities
local_share = 0.3           # Percentage of each locational share
local_exclusive = False     # Are local DERs available to the global system? True: yes, False: not
local_permanency = 0.75      # How often do local events occur (1.0 always, 0.0 never)


# ===== AGRREGATORS =====
num_aggregatoren = 100      # Total number of aggregators
dynamisierung = False       # Dynamic demand profiles (fluctuating demand according to SLP)
arbitage = True             # Aggregators can shift to other markets (arbitage)
arbitage_price = 100.0      # The arbitage price
arbitage_size = 20.0        # The size of the arbtage power plant (should theoretically be near infinity)
alpha = 1.0                 # The Demand-Factor
shuffle = True              # Shuffle the access of the Aggregators

# ===== DER SYSTEMS =====
# --- Number of Systems ---
num_sol = 65
num_bio = 10
num_gas = 5
num_wka = 20

# --- Global Pricing ---
spread = 0.2
usd_eur = 0.83
globale_strategie = "naiv"

# --- Biogas Dimensioning ---
# Power Distribution Parameters
bio_pn_mu = 0.66
bio_pn_sigma = 0.43
# Pricing
bio_ap_mu = 40.7 * usd_eur              # LVC + TC = 39.6 + 1.1 = 40.7
bio_ap_sigma = bio_ap_mu * spread
bio_bp_mu = 54.6 * usd_eur              # LCC + LFC = 39.2 + 15.4
bio_bp_sigma = bio_bp_mu * spread
bio_akp_mu = 24.0 / 10
bio_akp_sigma = bio_akp_mu * spread

# --- Gas Dimensioning ---
# Power Distribution Parameters
gas_pn_mu = 10.0
gas_pn_sigma = 2.0
# Pricing
gas_ap_mu = 33.3 * usd_eur              # LVC + TC = 32.2 + 1.1 = 33.3
gas_ap_sigma = gas_ap_mu * spread
gas_bp_mu = 15.7 * usd_eur              # LCC + LFC = 14.4 + 1.3 = 15.7
gas_bp_sigma = gas_bp_mu * spread
gas_akp_mu = 24.0 / 10                  # Normalized gas power plant costs
gas_akp_sigma = gas_akp_mu * spread

# --- Solar Dimensioning ---
solar_dynamik = False
# Power Distribution Parameters
sol_pn_mu = 0.27
sol_pn_sigma = 0.45
# Pricing
sol_ap_mu = 3.3 * usd_eur                         # LVC + TC = 0.0 + 3.3 = 3.3
sol_ap_sigma = sol_ap_mu * spread
sol_bp_mu = 59.9 * usd_eur                        # LCC + LFC = 51.2 + 8.7 = 59.9
sol_bp_sigma = sol_bp_mu * spread
sol_akp_mu = 0.0
sol_akp_sigma = sol_akp_mu * spread

# --- Wind Dimensioning ---
# Power Distribution Parameters
wka_pn_mu = 2.74
wka_pn_sigma = 0.85
# Pricing
wka_ap_mu = 2.5 * usd_eur                         # LVC + TC = 0.0 + 2.5 = 2.5
wka_ap_sigma = wka_ap_mu * spread
wka_bp_mu = 56.5 * usd_eur                        # LCC + LFC = 43.1 + 13.4 = 56.5
wka_bp_sigma = wka_bp_mu * spread
wka_akp_mu = 0.0
wka_akp_sigma = wka_akp_mu * spread
# Location für Weibull
# HH-Fuhlsbüttel, Quelle: A.K. Kaiser-Weiss et al.: "„Comparison of regional and global reanalysisnear-surface winds
# with station observations over German", DOI: 10.5194/asr-12-187-2015
wka_k = 2.62
wka_c = 3.83


def export():
    data = dict()
    data['global'] = dict(version=version, num_tage=num_tage,)
    data['local'] = dict(local=local,
                         locations=local_num_locations,
                         share=local_share,
                         exclusive=local_exclusive,
                         permanency=local_permanency )
    data['aggregatoren'] = dict(num_aggregatoren=num_aggregatoren,
                                dynamisierung=dynamisierung,
                                arbitage=arbitage,
                                arbitage_price=arbitage_price,
                                arbitage_size=arbitage_size,
                                alpha=alpha,
                                shuffle=shuffle, )
    data['ders'] = dict(solar=num_sol, wka=num_wka, gas=num_gas, bio=num_bio,
                        pricing=dict(spread=spread, usd_eur=usd_eur, strategie=globale_strategie))
    data['ders']['solar'] = dict(leistung=[sol_pn_mu, sol_pn_sigma],
                               ap=[sol_ap_mu, sol_ap_sigma],
                               bp=[sol_bp_mu, sol_bp_sigma],
                               akp=[sol_akp_mu, sol_akp_sigma])
    data['ders']['wka'] = dict(leistung=[wka_pn_mu, wka_pn_sigma],
                               ap=[wka_ap_mu, wka_ap_sigma],
                               bp=[wka_bp_mu, wka_bp_sigma],
                               akp=[wka_akp_mu, wka_akp_sigma],
                               weibull=[wka_k, wka_c])
    data['ders']['gas'] = dict(leistung=[gas_pn_mu, gas_pn_sigma],
                               ap=[gas_ap_mu, gas_ap_sigma],
                               bp=[gas_bp_mu, gas_bp_sigma],
                               akp=[gas_akp_mu, gas_akp_sigma])
    data['ders']['bio'] = dict(leistung=[bio_pn_mu, bio_pn_sigma],
                               ap=[bio_ap_mu, bio_ap_sigma],
                               bp=[bio_bp_mu, bio_bp_sigma],
                               akp=[bio_akp_mu, bio_akp_sigma])
    return data
