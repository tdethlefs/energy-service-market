import display.simdisplay as sd
from display.aggregators import show_aggregator_portfolio
from application import config
from aggregator.Aggregator import Aggregator
from registry.registryinstance import Registry
from helper.simexport import create_folder, write_dump_to_json
from helper.simdatacollect import export_kennzahlen
import time
import random
import hashlib

def current_milli_time():
    return int(round(time.time() * 1000))


if __name__ == '__main__':
    print('IRES APP')

    # Init variables
    registry = Registry.getInstance()
    aggregators = []
    aggregators_dump = dict()
    reg_dump = dict()

    # Init aggregators
    for i in range(config.num_aggregatoren):
        aggregator = Aggregator('Aggregator_' + str(i), registry=registry, locational=config.local)
        if config.arbitage is True:
            aggregator.erzeuge_default_anlage(name='AGG_' + str(i) + '_DEF_DER_1',
                                              pn=config.arbitage_size, price=config.arbitage_price)
        aggregators.append(aggregator)

    # Init localazation array, if neccessry
    if config.local is True:
        engpass_history = [random.random() < config.local_permanency for i in range(config.num_tage)]

    # Profiling start time
    start_milli = current_milli_time()

    # ===== Tagesläufe =====
    for tag in range(config.num_tage):
        print("Simuliere Tag " + str(tag))
        aggregators_dump[tag] = []
        registry.erzeuge_neue_tagesprofile(tag)

        # Shuffle the activation of the aggregators.
        if config.shuffle is True:
            random.shuffle(aggregators)

        # Localize or not
        localize = False
        if config.local is True and engpass_history[tag]:
            localize = True

        for aggregator in aggregators:
            aggregator.erzeuge_tagesprofil(tag)
            aggregator.generiere_fahrpläne(localize=localize)
            aggregators_dump[tag].append(aggregator.export(tag))

        reg_dump[tag] = registry.export(tag)

    # Profiling
    stop_milli = current_milli_time()
    diff = stop_milli - start_milli
    print("Exec Time: \n Total: " + str((diff / 1000)/60) + " Min \n Mean: " +
          str(diff / 1000 / config.num_tage) + " s/per day")

    # ===== Output =====
    h = hashlib.new('ripemd160')
    h.update(str(config.export()).encode("utf-8"))
    fingerprint = h.hexdigest()

    version = "-19-" + str(fingerprint[0:8])
    version.replace(".", "-")
    filepath = create_folder(version)

    direct_sig_plot = False

    sd.signaturplot_nominal_index(reg_dump[0], filename=filepath + 'registry-struktur-0.html', direct_plot=False)
    sd.signaturplot_nominal_index(reg_dump[config.num_tage-1], filename=filepath + 'registry-struktur-N.html', direct_plot=False)

    sd.index_util(reg_dump[0],
                  filename=filepath + 'registry-index-util-tag0.html', direct_plot=direct_sig_plot)

    #for x in range(250, 265):
    #    sd.index_util(reg_dump[x],
    #                  filename=filepath + 'registry-index-util-tag' + str(x) + '.html', direct_plot=direct_sig_plot)


    sd.registry_anteil(reg_dump, aggregators_dump,
                       filename=filepath + "registry-anteil.html", direct_plot=direct_sig_plot)
    sd.registry_anteil(reg_dump, aggregators_dump, relative_plot=True,
                       filename= filepath + 'registry-anteil-rel.html', direct_plot=direct_sig_plot)
    sd.anteil_anlagen_registry(reg_dump, filename=filepath + 'registry-anlagen.html', direct_plot=direct_sig_plot)
    sd.anteil_anlagen_registry(reg_dump, relative_plot=True, filename=filepath + 'registry-anlagen-rel.html', direct_plot=direct_sig_plot)
    
    sd.profitabilitatsentwicklung(reg_dump, filename=filepath + 'profitentwicklung.html', direct_plot=direct_sig_plot)
    sd.registry_index_entwicklung(reg_dump, filename=filepath + 'indexentwicklung.html')

    if config.local is True:
        sd.registry_index_entwicklung_local(reg_dump, filename=filepath + 'indexentwicklung-lokal.html', direct_plot=direct_sig_plot)
        write_dump_to_json(dict(engpaesse=str(engpass_history)), filepath + 'engpass.json')

    # ===== Persist =====
    #write_dump_to_json(reg_dump, filepath + 'reg_dump.json')
    #write_dump_to_json(aggregators_dump, filepath + 'agg_dump.json')
    write_dump_to_json(config.export(), filepath + 'config.json')

    write_dump_to_json(export_kennzahlen(reg_dump, aggregators_dump), filepath + 'kennzahlen.json')
    write_dump_to_json(export_kennzahlen(reg_dump, aggregators_dump, False), filepath + 'kennzahlen-uebersicht.json')



