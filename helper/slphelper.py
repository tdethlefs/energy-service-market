import csv
import numpy
from math import pow
from decimal import *


# Seasons:  0 = Übergang    |   Tagtyp: 0 = Werktag
#           1 = Sommer      |           1 = Samstag
#           2 = Winter      |           2 = Sonntag
def csvimporter(season=0, tagtyp=0):
    startindex = 1                  # Def: Übergang
    if season == 1: startindex = 4  # Sommer
    if season == 2: startindex = 7  # Winter
    profil = []
    with open('slph0.csv', newline='') as csvfile:
        datareader = csv.reader(csvfile, delimiter=';')
        for row in datareader:
            profil.append(float(row[startindex + tagtyp]))

    return profil


def dynamisierungsfunktion(tag, profil):

    if tag > 365: tag %= 365

    p1 = float(Decimal('-3.92e-10'))
    p2 = float(Decimal('3.2e-7'))
    p3 = float(Decimal('-7.02e-5'))
    p4 = float(Decimal('2.1e-3'))
    p5 = float(Decimal('1.24'))
    factor = p1 * pow(tag, 4) + p2 * pow(tag, 3) + p3 * pow(tag, 2) + p4 * tag + p5
    profil = map(lambda x: factor * x, profil)
    return profil
