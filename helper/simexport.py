from datetime import datetime
import json
import os
import errno


def create_folder(version=""):
    dt = datetime.now()
    sdate = dt.strftime("%y-%m-%d-%H-%M")
    filepath = 'runs/' + sdate + version + '/'

    if not os.path.exists(os.path.dirname(filepath)):
        try: os.makedirs(os.path.dirname(filepath))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST: raise

    return filepath


def write_dump_to_json(dump=None, filename='def_file.json'):
    with open(filename, "w") as f:
        json.dump(dump, f, indent=4)
    f.close()