import application.config as config
import numpy


def __get_avg_values(index_timerow, days):
    if index_timerow is not None:
        return dict(total_avg_index=numpy.sum(index_timerow)/days, max=numpy.max(index_timerow), min=numpy.min(index_timerow), sigma=numpy.std(index_timerow))
    else:
        return dict()


def _registry_share(reg_dump, aggregator_dump):
    # Die Anzahl der Tage
    days = len(reg_dump)
    # Der Energiebedarf eines Tages (Summe aller Aggregator-Tagesprofile)
    energiebedarf = numpy.zeros(days)
    # Der Anteil der Registry an dem Energiebedarf (Summe der DER-Profile in der Registry)
    registry_share = numpy.zeros(days)         # Summe der DER Profile
    # Summe des Potentials in der Registry (Summe der einzelnen DER Potentiale über den Tag)
    registry_potential = numpy.zeros(days)

    for day in aggregator_dump:
        iday = int(day)
        for aggregator in aggregator_dump[day]:
            energiebedarf[iday] += numpy.sum(aggregator['tagesprofil'])

    for day in reg_dump:
        iday = int(day)
        for der_key, der in reg_dump[day].items():
            registry_potential[iday] += numpy.sum(der['potential'])
            registry_share[iday] += numpy.sum(der['gesamtfahrplan'])

    # Relative assignments
    registry_potential = registry_potential / energiebedarf
    registry_share = registry_share / energiebedarf
    energiebedarf = energiebedarf / energiebedarf

    return dict(potential=numpy.sum(registry_potential)/days,
                share=numpy.sum(registry_share)/days,
                energiebedarf=numpy.sum(energiebedarf)/days)


def _mean_index(reg_dump, location=None, detailed=True):
    # Die Anzahl der Tage
    days = len(reg_dump)

    avg_index = numpy.zeros(days)
    wka_index = numpy.zeros(days)
    sol_index = numpy.zeros(days)
    bio_index = numpy.zeros(days)
    gas_index = numpy.zeros(days)

    avg_duration = [0, 0]
    wka_duration = [0, 0]
    sol_duration = [0, 0]
    bio_duration = [0, 0]
    gas_duration = [0, 0]

    for day in reg_dump:
        iday = int(day)
        for key, der in reg_dump[day].items():
            if location is None or der['location'] == int(location):
                der_index = der['aktivierungspreis'] + der['arbeitspreis'] + der['bereitstellungspreis']
                duration = der['telemetry']['duration']
                trades = der['telemetry']['numtrades']

                avg_index[iday] += der_index
                avg_duration[0] += duration
                avg_duration[1] += trades

                if der['typ'] is 'WKA':
                    wka_index[iday] += der_index
                    wka_duration[0] += duration
                    wka_duration[1] += trades
                if der['typ'] is 'PV':
                    sol_index[iday] += der_index
                    sol_duration[0] += duration
                    sol_duration[1] += trades
                if der['typ'] is 'Gas':
                    gas_index[iday] += der_index
                    gas_duration[0] += duration
                    gas_duration[1] += trades
                if der['typ'] is 'Bio':
                    bio_index[iday] += der_index
                    bio_duration[0] += duration
                    bio_duration[1] += trades

    if location is None:
        total_ders = config.num_gas + config.num_bio + config.num_wka + config.num_sol
        avg_index = avg_index / total_ders
        wka_index = wka_index / config.num_wka
        sol_index = sol_index / config.num_sol
        bio_index = bio_index / config.num_bio
        gas_index = gas_index / config.num_gas
    else:
        cnt_wka = len({k: v for k, v in reg_dump[0].items() if v['typ'] is 'WKA' and v['location'] == int(location)})
        cnt_sol = len({k: v for k, v in reg_dump[0].items() if v['typ'] is 'PV' and v['location'] == int(location)})
        cnt_bio = len({k: v for k, v in reg_dump[0].items() if v['typ'] is 'Bio' and v['location'] == int(location)})
        cnt_gas = len({k: v for k, v in reg_dump[0].items() if v['typ'] is 'Gas' and v['location'] == int(location)})

        total_ders = cnt_wka + cnt_bio + cnt_sol + cnt_gas
        avg_index = avg_index / total_ders

        if cnt_wka > 0: wka_index = wka_index / cnt_wka
        else: wka_index = None
        if cnt_sol > 0: sol_index = sol_index / cnt_sol
        else: sol_index = None
        if cnt_bio > 0: bio_index = bio_index / cnt_bio
        else: bio_index = None
        if cnt_gas > 0: gas_index = gas_index / cnt_gas
        else: gas_index = None

    total = __get_avg_values(avg_index, days)
    wka = __get_avg_values(wka_index, days)
    sol = __get_avg_values(sol_index, days)
    bio = __get_avg_values(bio_index, days)
    gas = __get_avg_values(gas_index, days)

    total['avg_dur'] = avg_duration[0] / avg_duration[1]
    if wka_duration[1] > 0 : wka['avg_dur'] = wka_duration[0] / wka_duration[1]
    if sol_duration[1] > 0 :sol['avg_dur'] = sol_duration[0] / sol_duration[1]
    if bio_duration[1] > 0 :bio['avg_dur'] = bio_duration[0] / bio_duration[1]
    if gas_duration[1] > 0 :gas['avg_dur'] = gas_duration[0] / gas_duration[1]

    if detailed is True:
        return dict(total=total, wka=wka, sol=sol, bio=bio, gas=gas, ders=total_ders)
    else:
        return dict(total=total, ders=total_ders)


def _market_power(reg_dump, aggregator_dump, location=None, detailed=True):
    """ Calculating Alpha, the Herfindahl Hirschman Index for Market power  and the shares of DER Systems"""
    der_energy = dict()
    registry_energy = 0
    hhi = 0
    shares = []

    days = len(reg_dump)
    energiebedarf = numpy.zeros(days)
    registry_potential = numpy.zeros(days)

    for day in reg_dump:
        iday = int(day)
        for der_key, der in reg_dump[day].items():
            if location is None or der['location'] == int(location):
                energie = numpy.sum(der['gesamtfahrplan']).item()
                if der_key in der_energy: der_energy[der_key] += energie
                else: der_energy[der_key] = energie
                registry_energy += energie
                registry_potential[iday] += numpy.sum(der['potential'])

    if registry_energy > 0:
        hhi = sum(list(map(lambda v: pow(v[1]/registry_energy, 2), der_energy.items()))) * 10000
        shares = list(map(lambda v: [v[0], v[1]/registry_energy], der_energy.items()))

    for day in aggregator_dump:
        iday = int(day)
        for aggregator in aggregator_dump[day]:
            if location is None or aggregator['location'] == int(location):
                energiebedarf[iday] += numpy.sum(aggregator['tagesprofil'])

    registry_potential = numpy.sum((registry_potential / energiebedarf)) / days

    if detailed is True:
        return dict(hhi=hhi, location=location, potential=registry_potential, shares=shares)
    else:
        return dict(hhi=hhi, location=location, potential=registry_potential)


def _aggregator_eigenanteil(aggregator_dump, location=None, detailed=True):
    aggregator_eigenanteil = dict()
    days = len(aggregator_dump)

    for day in aggregator_dump:
        iday = int(day)
        for aggregator in aggregator_dump[day]:
            if location is None or aggregator['location'] == int(location):
                name = aggregator['name']
                telemetrie = aggregator['telemetrie']
                eigenanteil = telemetrie['eigenanteil']
                if name in aggregator_eigenanteil: aggregator_eigenanteil[name] += eigenanteil * 1/days
                else: aggregator_eigenanteil[name] = eigenanteil * 1/days

    avg_eigenanteil = sum(aggregator_eigenanteil.values()) / len(aggregator_eigenanteil)

    if detailed is True:
        return dict(avg=avg_eigenanteil, aggregatoren=aggregator_eigenanteil)
    else:
        return dict(avg=avg_eigenanteil)


def export_kennzahlen(reg_dump, aggregator_dump, detailed=True):
    result = dict()

    global_results = dict()
    global_results['index'] = _mean_index(reg_dump, None, detailed)
    global_results['power'] = _market_power(reg_dump=reg_dump, aggregator_dump=aggregator_dump, detailed=detailed)
    global_results['eigenanteil'] = _aggregator_eigenanteil(aggregator_dump=aggregator_dump, detailed=detailed)
    result['global'] = global_results

    if config.local is True:
        local_results = dict()
        location = 0

        while location <= config.local_num_locations:
            location_result = dict()
            location_result['index'] = _mean_index(reg_dump, location, detailed)
            location_result['power'] = _market_power(reg_dump, aggregator_dump, location, detailed)
            location_result['eigenanteil'] = _aggregator_eigenanteil(aggregator_dump, location, detailed)
            local_results[str(location)] = location_result
            location = location + 1

        result['local'] = local_results

    return result
