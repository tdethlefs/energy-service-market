import numpy.random as npyrandom
from application import config
import time

random_init = False

def uniformdist():
    global random_init
    if not random_init:
        npyrandom.seed(time.gmtime())
        random_init = True
    return npyrandom.rand()


def determine_location(num_locations=0, share=0.0):
    rnum = uniformdist()
    loc = 0

    # A global entity:
    if rnum >= (num_locations * share):
        return loc

    # A local entity
    while rnum > loc * share:
        loc = loc + 1

    return loc


# =========================================================
# =========== Localizer rules for the registry ============
# =========================================================
def check_local_condition(der, location):
    """ Three rules must be employed:
    1st Rule: If localize is None return always True, as it doesn't matter.
    2nd Rule: If config.local_exclusive is True only return True if der.location == localize, False otherwise.
    3rd Rule: If config.local_exclusive is False return True if localize is 0 OR der.location == localize.
    """

    # Rule No. 1, Rule No. 2 and Rule No. 2 and 3 combined.
    if (location is None) or (der.location == int(location)) or (config.local_exclusive is False and location == 0):
        return True
    else:
        return False
