from math import pi, sin, cos, acos, sqrt, pow


def __rad(degree):
    return pi * degree/180


def __sign(number):
    if number > 0: return 1
    if number < 0: return -1
    return 0

'''
Function to approximate the sunrise and sunset time of a location defined
by the longitude and latitude and the day of the year. Remeber, that it not
includes daylight saving time!
Sources: https://www.r-bloggers.com/approximate-sunrise-and-sunset-times/
Teets, D.A.: "Predicting sunrise and sunset times" in The College
Mathematics Journal 34(4), pages 317 - 321, 2003
'''
def sun_times(day, lat, long, timezone=0):
    if day > 365: day %= 365

    r_earth = 6378          # earth's radius
    r_earth_sun = 149598000 # mean distance between sun and earth

    epsilon = __rad(23.45)  # Radians between xy-plane and eliptic plane
    l = __rad(lat)          # lat in radians

    theta = 2 * pi / 365.25 * (day - 80)
    zs = r_earth_sun * sin(theta) * sin(epsilon)
    rp = sqrt(pow(r_earth_sun, 2) - pow(zs, 2))

    ac = (r_earth - zs * sin(l)) / (rp * cos(l))
    t0 = 1440 / (2 * pi) * acos(ac)

    # adjustement for sun's orbit
    th = t0 + 5

    # noon adjust for orbit eccentricity
    n = 720 - 10 * sin(4*pi*(day-80)/365.25) + 8*sin(2*pi*day/365.25)

    sunrise = (n - th + timezone) / 60
    sunset = (n + th + timezone) / 60

    return dict(sunrise=sunrise, sunset=sunset)


def dectime_to_time(decimaltime):
    hours = int(decimaltime)
    minutes = (decimaltime * 60) % 60
    seconds = (decimaltime * 3600) % 60

    return [hours, minutes, seconds]


def dectime_to_quarter(decimaltime):
    hours = int(decimaltime)
    minutes = (decimaltime * 60) % 60

    return 4 * hours + int(minutes / 15)


def create_day_timerow(day):
    return [i for i in range(96)]