import numpy
import queue
from itertools import groupby


# ========================================
# ===== Offers berechnen (Threading?) =====
# ========================================
def calc_offer(heap, offer, defizitplan, unten, oben):
    # Berechnung des Angebotsanteils an der Gesamtlösung
    derfahrplan = __truc_angebot(offer['angebot'], defizitplan, unten, oben)
    energie = numpy.sum(derfahrplan)

    # Nicht beitragende Anlagen rauswerfen.
    if energie > 0.0:
        # Berechnung der Einzelpreise
        arbeitspreis = energie * offer['preise']['arbeitspreis']
        bereitstellungspreis = energie * offer['preise']['bereitstellungspreis']
        aktivierungspreis = offer['preise']['aktivierungspreis']

        # Preisindex für Auswahl
        index = arbeitspreis / energie \
                + bereitstellungspreis / energie \
                + aktivierungspreis / (oben - unten)

        # Einzelpreise zur nachverfolgung
        kosten = dict(arbeitspreis=arbeitspreis,
                      bereitstellungspreis=bereitstellungspreis,
                      aktivierungspreis=aktivierungspreis)

        # PriorityQueue Eintrag erzeugen
        offertuple = PriorityEntry(index, {'index': index,
                                           'offer': offer,
                                           'optfahrplan': derfahrplan.tolist(),
                                           'kosten': kosten})

        # heapq.heappush(heap, offertuple)  # Daten in den Heap packen
        heap.put(offertuple)


# =========================================
# ===== Fahrplan aus Angebot erzeugen =====
# =========================================
def __truc_angebot(angebotsfahrplan, zielfahrplan, start, end):
    """
        Berechnung des Angebotsanteils an dem Zielfahrplan
            Dabei Fallunterscheidung:
            1. Angebot trägt nicht bei: Fahrplan auf null setzen
            2. Angebot übersteigt anforderung: nur die anforderung nehmen
            3. Anforderung höher als angebot: das angebot nehmen
    """
    fahrplan = numpy.zeros(96)
    fahrplan[start:end] = numpy.array([angebotsfahrplan[start:end], zielfahrplan[start:end]]).min(axis=0)
    return fahrplan


class PriorityEntry(object):
    """ Klasse für die Priority Queue, um die Prioritäten richtig zu setzen.
    """
    def __init__(self, priority, data):
        self.data = data
        self.priority = priority

    def __lt__(self, other):
        return self.priority < other.priority


# =========================================================
# ===== Bestes Angebot für eine Zeitscheibe berechnen =====
# =========================================================
def find_minimum(defizitplan, offers, unten, oben):
    pq = queue.PriorityQueue()

    for key, offer in offers.items():
        calc_offer(pq, offer, defizitplan, unten, oben)

    # Angebot heraussuchen und zurückgeben
    if not pq.empty():
        bestoffer = pq.get()  # Python Heap: Min-heap by definition
        return bestoffer.data
    else:
        return None


# =============================================
# ===== Finde Nullstellen für Optimierung =====
# =============================================
def finde_nullstellen(fahrplan):
    binarylist = list(map(lambda x: True if x > 0.0 else False, fahrplan))
    bereiche = []
    start = 0
    for group in groupby(binarylist):
        sublist = list(group[1])
        if sublist[0] is True:
            bereiche.append([start, start + len(sublist)])
        start += len(sublist)
    return bereiche


# ======================================
# ===== Finde Lösbare Bereiche =====
# ======================================
def loesbare_bereiche(defizitfahrplan, offers):
    offer_pool = numpy.zeros(96)
    for key, offer in offers.items():
        offer_pool = offer_pool + numpy.asarray(offer['angebot'])

    offer_pool_abdeckung = offer_pool * numpy.asarray(defizitfahrplan)
    bereiche = finde_nullstellen(offer_pool_abdeckung.tolist())
    return bereiche


