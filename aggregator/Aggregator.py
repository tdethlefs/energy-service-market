import application.config as config
import numpy

from operator import sub, add, mul
from derssystems.derFactory import create_stochastic_conventional, create_arbitage_conventional
from helper.slphelper import csvimporter, dynamisierungsfunktion
from registry.registryinstance import Registry
from aggregator.scheduletools import finde_nullstellen, find_minimum, loesbare_bereiche
from helper.LocationHelper import determine_location


# ===== Default Aggregator =====
class Aggregator:

    def __init__(self, name, registry=None, locational=False):
        self.name = name                        # Name des Aggregators
        self.tagesprofil = []                   # Das Tagesprofil als Summe des Tages
        self.gesamtfahrplan = []                # Der Gesamtfahrplan des Tages
        self.portfolio = dict()                 # Nutzt DerEintrag (s.u.)
        self.kosten = dict()                    # Kostenstruktur
        self.defaultder = None                  # Default DER, um grundlegenden Bedarf zu decken.
        self.registry = Registry.getInstance()  # Dependency Injection für die Registry
        self.basisprofil = csvimporter()        # SLP Basisprofil import

        if locational is True:                  # Der Orts-Parameter
            self.location = determine_location(config.local_num_locations, config.local_share)
        else:
            self.location = 0

    # ==========================================================
    # ===== Problem-Erzeugung für einen Tag (Hauptroutine) =====
    # ==========================================================
    def erzeuge_tagesprofil(self, tag):
        # print(str(self.name) + ' - Erzeuge Tagesprofil Tag ' + str(tag))

        # Tagesanfoderungsprofik erzeugen
        if self.basisprofil is None:
            self.tagesprofil = self.__tagesprofil_rechteck(10)
        else:
            if config.dynamisierung is True: dyn_faktor_profil = dynamisierungsfunktion(tag, self.basisprofil.copy())
            else: dyn_faktor_profil = self.basisprofil.copy()

            # Achtung: SLPs werden in kW angegeben, damit ist der Verbrauch in kWh umzurechnen!
            # d.h. 1 / 4000
            # Alpha := Knappheitsfaktor
            self.tagesprofil = list(map(lambda x: config.alpha * 4 * (x / 4000) * 1500, dyn_faktor_profil))

        # Portfolio zurücksetzen
        self.portfolio = dict()

        # Default DER erzeugen
        if self.defaultder is not None:
            self.defaultder.erzeuge_tagesprofil(tag)
            self.portfolio[self.defaultder.name] = EnergyService(name=self.defaultder.name,
                                                                 potential=self.defaultder.potential,
                                                                 preise=self.defaultder.get_preise_dict())

        # Gesamtfahrplan auf null
        self.gesamtfahrplan = [0.0 for i in range(96)]

    # ==========================================================
    # ===== Fahrplanerzeugung für einen Tag (Hauptroutine) =====
    # ==========================================================
    def generiere_fahrpläne(self, localize=False):
        # print(str(self.name) + ' - Generiere Fahrpläne')
        defizitplan = list(map(sub, self.tagesprofil, self.gesamtfahrplan))
        energiedefizit_gesamt = sum(defizitplan)        # Das gesamte Energiedefizit
        offers = dict()                                 # Angebots Dictionary

        # Freies Potential im Portfolio identifizieren, d.h. Potential abzgl. dem schon gewählten Fahrplan
        for key, es in self.portfolio.items():
            offers[key] = es.erzeuge_angebot()

        # The location parameter
        if localize is True:
            location = self.location
        else:
            location = None

        # Angebote aus der Registry dem offers-array hinzufügen
        offers_reg = self.registry.get_angebote(localize=location)

        for key, offer in offers_reg.items():
            offers[key] = offer

        # Minimale Kosten rausholen - Merit Order List Verfahren
        # Solange der totale Bedarf nicht gedeckt ist, das günstige Angebot nehmen
        # Dann wieder in zusammenhängende Bereich unterteilen und für jeden
        # das günstigste Angebot annehmen
        # In zusammenhängende bereiche unterteilen (für die Umlage der aktivierungskosten)
        bereiche = finde_nullstellen(defizitplan)

        # ===== Bereichsoptimierung =====
        # Für jeden Bereich die Preise kalkulieren
        while len(offers) > 0 and energiedefizit_gesamt > 0.01 and len(bereiche) > 0:
            for bereich in bereiche:
                unten = bereich[0]
                oben = bereich[1]

                # Das beste Ergebnis heraussuchen.
                bestoffer = find_minimum(defizitplan, offers, unten, oben)

                # ===== Buchen =====
                if bestoffer is not None:
                    name = bestoffer['offer']['name']
                    if name in self.portfolio:
                        self.portfolio[name].fahrplan = list(map(add, self.portfolio[name].fahrplan,
                                                                 bestoffer['optfahrplan']))
                        self.portfolio[name].kosten['aktivierungspreis'] += bestoffer['kosten']['aktivierungspreis']
                        self.portfolio[name].kosten['bereitstellungspreis'] += bestoffer['kosten']['bereitstellungspreis']
                        self.portfolio[name].kosten['arbeitspreis'] += bestoffer['kosten']['arbeitspreis']
                    else:
                        self.portfolio[name] = EnergyService(name=name, fahrplan=bestoffer['optfahrplan'],
                                                             kosten=bestoffer['kosten'], index=bestoffer['index'])

                    offers[name] = self.portfolio[name].erzeuge_angebot()

            # ===== Gesamtfahrplan anpassen =====
            gesamtfahrplan_neu = numpy.zeros(96)
            for key, der in self.portfolio.items():
                gesamtfahrplan_neu = gesamtfahrplan_neu + numpy.asarray(der.fahrplan)

            npy_defizitplan = numpy.asarray(self.tagesprofil) - gesamtfahrplan_neu
            npy_defizitplan[npy_defizitplan < 0.0] = 0.0
            energiedefizit_gesamt = numpy.sum(npy_defizitplan)

            self.gesamtfahrplan = gesamtfahrplan_neu.tolist()
            defizitplan = npy_defizitplan.tolist()

            # Bereiche anpassen
            bereiche = loesbare_bereiche(defizitplan, offers)

        for key, es in self.portfolio.items():
            if key is not self.defaultder.name:
                succ = self.registry.angebot_buchen(self.name, key, es.fahrplan, es.kosten)
                if succ is False: print("ERR! beim buchen!")

    # =========================================
    # ===== erzeugen einer Default anlage =====
    # =========================================
    def erzeuge_default_anlage(self, name, pn=0.0, price=0.0):
        if price != 0.0:
            der = create_arbitage_conventional(name=name, pn=pn, price=price)
        else:
            der = create_stochastic_conventional(name=name, pn=pn)

        self.defaultder = der

    # =======================
    # ===== Dict-Export =====
    # =======================
    def export(self, day):
        data = dict()
        data['name'] = self.name
        data['day'] = day
        data['tagesprofil'] = self.tagesprofil
        data['gesamtfahrplan'] = self.gesamtfahrplan
        # data['portfolio'] = dict()
        # for key, service in self.portfolio.items():
        #    data['portfolio'][service.name] = service.export()
        self.__gesamtkosten_bestimmen()
        data['kosten'] = self.kosten
        data['telemetrie'] = self.__telemetrie_daten()
        data['location'] = self.location
        return data

    # =========================================================
    # ===== Telemetrie: Gesamtkosten des Tages bestimmen  =====
    # =========================================================
    def __gesamtkosten_bestimmen(self):
        # Reset Gesamtkosten
        self.kosten = dict(arbeitspreis=0.0, bereitstellungspreis=0.0, aktivierungspreis=0.0)
        # Loop über alle Elemente des Portfolios
        for key in self.portfolio:
            der = self.portfolio[key]
            kosten_dict = der.kosten
            self.kosten['arbeitspreis'] += kosten_dict['arbeitspreis']
            self.kosten['bereitstellungspreis'] += kosten_dict['bereitstellungspreis']
            self.kosten['aktivierungspreis'] += kosten_dict['aktivierungspreis']
        # Gesamtkosten bestimmen
        self.kosten['gesamt'] = self.kosten['arbeitspreis'] \
                                + self.kosten['bereitstellungspreis'] \
                                + self.kosten['aktivierungspreis']

    # =========================================================
    # ===== Anteile bestimmen  =====
    # =========================================================
    def __telemetrie_daten(self):
        data = dict()
        gesamtenergie = sum(self.gesamtfahrplan)
        gesamtenergiebedarf = sum(self.tagesprofil)
        # Eigenanteil
        der = self.portfolio[self.defaultder.name]
        fahrplan = der.fahrplan
        data['eigenanteil'] = sum(fahrplan) / gesamtenergie
        # Abdeckungsgrad
        data['abdeckungsgrad'] = gesamtenergie / gesamtenergiebedarf

        return data

    # ----- ONLY FOR DEBUGGING! -----
    def __tagesprofil_rechteck(self, power):
        return [ power / 4 for i in range(96)]


# ===== Energy Service =====
class EnergyService(object):

    def __init__(self, name='', fahrplan=None, potential=None, kosten=None, preise=None, index=0.0):
        self.name = name
        if fahrplan is None: self.fahrplan = [0.0 for i in range(96)]
        else: self.fahrplan = fahrplan
        if potential is None: self.potential = [0.0 for i in range(96)]
        else: self.potential = potential
        if kosten is None: self.kosten = dict(arbeitspreis=0.0, bereitstellungspreis=0.0, aktivierungspreis=0.0)
        else: self.kosten = kosten
        if preise is None: self.preise = dict(arbeitspreis=0.0, bereitstellungspreis=0.0, aktivierungspreis=0.0)
        else: self.preise = preise
        if index != 0.0: self.index = index
        else: self.index = 0.0

    def erzeuge_angebot(self):
        offer = dict()
        offer['name'] = self.name
        offer['angebot'] = list(map(sub, self.potential, self.fahrplan))
        offer['preise'] = dict()
        offer['preise']['arbeitspreis'] = self.preise['arbeitspreis']
        offer['preise']['bereitstellungspreis'] = self.preise['bereitstellungspreis']
        offer['preise']['aktivierungspreis'] = self.preise['aktivierungspreis']
        return offer.copy()

    def export(self):
        data = dict()
        data['name'] = self.name
        data['fahrplan'] = self.fahrplan
        data['potential'] = self.potential
        data['kosten'] = self.kosten
        data['preise'] = self.preise
        data['index'] = self.index
        return data
