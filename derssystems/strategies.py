

def naive_strategie(auslastung=0.0, preise=dict(), opreise=dict(), alpha=0.05):

    for key, value in preise.items():
        if auslastung >= 1.0: preise[key] = value * (1 + alpha)
        else:
            if not value <= opreise[key]:
                preise[key] = (1 - alpha) * value

    return preise


def naive_strategie_space(auslastung=0.0, preise=dict(), opreise=dict(), alpha=0.05):

    for key, value in preise.items():
        if auslastung >= 0.8: preise[key] = value * (1 + alpha)
        else:
            if not value <= opreise[key]:
                preise[key] = (1 - alpha) * value

    return preise


def naive_strategie_no_limits(auslastung=0.0, preise=dict(), opreise=dict(), alpha=0.05):
    for key, value in preise.items():
        if auslastung >= 1.0:
            preise[key] = value * (1 + alpha)
        else:
            preise[key] = (1 - alpha) * value

    return preise
