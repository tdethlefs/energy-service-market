import time
import numpy.random as npyrandom
import application.config as ac
from derssystems.ders import DerSystem, Solaranlage, Windkraftanlage

random_init = False


def gauss(mean, sigma):
    global random_init
    if not random_init:
        npyrandom.seed(time.gmtime())
        random_init = True
    return npyrandom.normal(mean, sigma)


def create_stochastic_biogas(name='', pn=0.0,
                             pn_mu=ac.bio_pn_mu, pn_sigma=ac.bio_pn_sigma,
                             ap_mu=ac.bio_ap_mu, ap_sigma=ac.bio_ap_sigma,
                             bp_mu=ac.bio_bp_mu, bp_sigma=ac.bio_bp_sigma,
                             akp_mu=ac.bio_akp_mu, akp_sigma=ac.bio_akp_sigma,
                             strategie='naiv',
                             locational=False):
    if pn == 0.0: pn = npyrandom.lognormal(pn_mu, pn_sigma)
    d = DerSystem(name, 'Bio', abs(pn),
                  dict(arbeitspreis=abs(gauss(ap_mu, ap_sigma)),
                       bereitstellungspreis=abs(gauss(bp_mu, bp_sigma)),
                       aktivierungspreis=abs(gauss(akp_mu, akp_sigma))), strategie=strategie, locational=locational)
    return d


# Gaskraftwerke um den Faktor 10 runterskalieren, um Bed. zu matchen
def create_stochastic_conventional(name='', pn=0.0,
                                   pn_mu=ac.gas_pn_mu, pn_sigma=ac.gas_pn_sigma,
                                   ap_mu=ac.gas_ap_mu, ap_sigma=ac.gas_ap_sigma,
                                   bp_mu=ac.gas_bp_mu, bp_sigma=ac.gas_bp_sigma,
                                   akp_mu=ac.gas_akp_mu, akp_sigma=ac.gas_akp_sigma,
                                   strategie='naiv',
                                   locational=False):
    if pn == 0.0: pn = gauss(pn_mu, pn_sigma)
    d = DerSystem(name, 'Gas', abs(pn),
                  dict(arbeitspreis=abs(gauss(ap_mu, ap_sigma)),
                       bereitstellungspreis=abs(gauss(bp_mu, bp_sigma)),
                       aktivierungspreis=abs(gauss(akp_mu, akp_sigma))), strategie=strategie, locational=locational)
    return d


def create_arbitage_conventional(name='', pn=0.0, price=0.0):
    if pn == 0.0: pn = 100  # Default 100 MW Power Plant
    d = DerSystem(name, 'ARBITAGE', pn,
                  dict(arbeitspreis=price,
                       bereitstellungspreis=0.0,
                       aktivierungspreis=0.0))
    return d


def create_stochastic_solar(name='',
                            pn_mu=ac.sol_pn_mu, pn_sigma=ac.sol_pn_sigma,
                            ap_mu=ac.sol_ap_mu, ap_sigma=ac.sol_ap_sigma,
                            bp_mu=ac.sol_bp_mu, bp_sigma=ac.sol_bp_sigma,
                            akp_mu=ac.sol_akp_mu, akp_sigma=ac.sol_akp_sigma,
                            strategie='naiv',
                            locational=False):
    pn = gauss(pn_mu, pn_sigma)  # Lognormal Process to cover houshold as well as solar fields
    d = Solaranlage(name, 'PV', abs(pn),
                  dict(arbeitspreis=abs(gauss(ap_mu, ap_sigma)),
                       bereitstellungspreis=abs(gauss(bp_mu, bp_sigma)),
                       aktivierungspreis=abs(gauss(akp_mu, akp_sigma))), strategie=strategie, locational=locational)
    return d


def create_stochastic_wind(name='',
                           pn_mu=ac.sol_pn_mu, pn_sigma=ac.sol_pn_sigma,
                           ap_mu=ac.sol_ap_mu, ap_sigma=ac.sol_ap_sigma,
                           bp_mu=ac.sol_bp_mu, bp_sigma=ac.sol_bp_sigma,
                           akp_mu=ac.sol_akp_mu, akp_sigma=ac.sol_akp_sigma,
                           strategie='naiv',
                           locational=False):
    pn = npyrandom.lognormal(pn_mu, pn_sigma)  # Gauss Process, as the largest WKA is about 9.5 MW
    d = Windkraftanlage(name, 'WKA', abs(pn),
                  dict(arbeitspreis=abs(gauss(ap_mu, ap_sigma)),
                       bereitstellungspreis=abs(gauss(bp_mu, bp_sigma)),
                       aktivierungspreis=abs(gauss(akp_mu, akp_sigma))), strategie=strategie, locational=locational)
    return d
