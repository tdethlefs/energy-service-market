import random
import time
import numpy
from math import sin, pi
import application.config as config
import derssystems.strategies as strategies
from helper.DateHelper import sun_times, dectime_to_quarter
from helper.LocationHelper import determine_location


# ===== Das Default DER System =====
class DerSystem:

        # ==================================================
        # ===== Konstruktor Methode für das DER System =====
        # ==================================================
        def __init__(self, name='', typ='Gas', nominalleistung=0.0,
                     preise=dict(arbeitspreis=0.0, bereitstellungspreis=0.0, aktivierungspreis=0.0),
                     strategie=None, locational=None):
            self.name = name
            self.typ = typ
            self.nominalleistung = nominalleistung # in MW!
            self.gesamtfahrplan = []
            self.fahrplaene = {}
            self.angebot = []
            self.potential = []
            self.buchungen = {}
            self.ledger = {}
            self.tradelength = []
            self.arbeitspreis = preise['arbeitspreis']
            self.bereitstellungspreis = preise['bereitstellungspreis']
            self.aktivierungspreis = preise['aktivierungspreis'] * nominalleistung
            self.originalpreise = preise
            self.saturated = True  # Anlage hat kein angebot mehr
            self.strategie = strategie

            if locational is True:  # Der Orts-Parameter
                self.location = int(determine_location(config.local_num_locations, config.local_share))
            else:
                self.location = 0

        # ===================================================
        # ===== Erzeugen der Tagesprofile und -angebote =====
        # ===================================================
        def erzeuge_tagesprofil(self, tag):
            # Eval der Preise, aber erst ab dem 2. Tag!
            if self.strategie is not None:
                self.eval_prices()

            # Erzeuge die Angebote
            self.angebot = (numpy.ones(96) * (self.nominalleistung / 4)).tolist()

            # Reset Anlage
            self._reset_anlage()

        # ==============================================================
        # ===== Zurücksetzen am Anfang eines neuen Tagesintervalls =====
        # ==============================================================
        def _reset_anlage(self):
            if sum(self.angebot) > 0.0: self.saturated = False
            else: self.saturated = True
            self.gesamtfahrplan = numpy.zeros(96).tolist()
            self.fahrplaene = {}
            self.buchungen = {}
            self.ledger = {}
            self.potential = self.angebot.copy()

        # ==========================================================
        # ===== Buchen der Anlage durch Aggregator via Registy =====
        # ==========================================================
        def buche_anlage(self, aggregatorid, profil, bezahlung):
            """
            TODO: Consec. Buchen möglich machen!
            :param aggregatorid: Der buchende Aggregator
            :param profil: Das Profil, welches geseteuert werden soll
            :param bezahlung: der zu bezahlende Betrag
            :return: True für valide Buchungen, False für fehlgeschlagene Buchungen
            """
            # Validitätscheck, ob das profil überhaupt noch passt.
            npy_angebot = numpy.asarray(self.angebot)
            npy_profil = numpy.asarray(profil)
            neuesangebot = npy_angebot - npy_profil
            zero_check = numpy.any(x < 0.0 for x in neuesangebot)

            # ist die Buchung so möglich und der Aggregator hat
            # noch keine Buchung vorgenommen, dann laufe durch.
            if zero_check and (aggregatorid not in self.buchungen):
                self.angebot = neuesangebot.tolist()    # Gesamtangebot anpassen
                self.buchungen[aggregatorid] = profil   # Buchung in das Buchungen-Dict packen
                self.ledger[aggregatorid] = bezahlung   # Bezahlung in das Ledger-Dict packen
                self.tradelength.append(numpy.count_nonzero(npy_profil))

                if numpy.sum(neuesangebot) <= 0.0:      # Check, ob gesättigt, d.h. keine Energie mehr vorhanden
                    self.saturated = True
                return True                             # Successful booking
            return False                                # Someting went wrong

        # ====================================
        # ===== Erzeugung eines Angebots =====
        # ====================================
        def erzeuge_offer(self):
            offer = dict()
            offer['name'] = self.name
            offer['angebot'] = self.angebot
            offer['preise'] = dict()
            offer['preise']['arbeitspreis'] = self.arbeitspreis
            offer['preise']['bereitstellungspreis'] = self.bereitstellungspreis
            offer['preise']['aktivierungspreis'] = self.aktivierungspreis
            return offer.copy()     # Deep Copy!

        def sum_gesamtfahrplan(self):
            gesamtfahrplan = numpy.zeros(96)
            for key, buchung in self.buchungen.items():
                gesamtfahrplan = numpy.asarray(buchung) + gesamtfahrplan
            self.gesamtfahrplan = gesamtfahrplan.tolist()

        # ===============================
        # ===== Anpassen der Preise =====
        # ===============================
        def eval_prices(self):
            """
            Erfordert eine gesetzte Preis-Strategie in self.strategie
            :return: None
            """
            potential = sum(self.potential)

            if potential > 0:
                abgerufen = sum(self.gesamtfahrplan)
                auslastung = abgerufen / potential
                if self.strategie == 'naiv':
                    preisschema = strategies.naive_strategie(auslastung=auslastung,
                                                             preise=dict(aktivierungspreis=self.aktivierungspreis,
                                                                         arbeitspreis=self.arbeitspreis,
                                                                         bereitstellungspreis=
                                                                         self.bereitstellungspreis),
                                                             opreise=self.originalpreise)
                if self.strategie == 'naiv-no-limit':
                    preisschema = strategies.naive_strategie_no_limits(auslastung=auslastung,
                                                                       preise=dict(aktivierungspreis=
                                                                                   self.aktivierungspreis,
                                                                                   arbeitspreis=
                                                                                   self.arbeitspreis,
                                                                                   bereitstellungspreis=
                                                                                   self.bereitstellungspreis),
                                                                       opreise=self.originalpreise)
                if self.strategie == 'naiv-space':
                    preisschema = strategies.naive_strategie_space(auslastung=auslastung,
                                                                       preise=dict(aktivierungspreis=
                                                                                   self.aktivierungspreis,
                                                                                   arbeitspreis=
                                                                                   self.arbeitspreis,
                                                                                   bereitstellungspreis=
                                                                                   self.bereitstellungspreis),
                                                                       opreise=self.originalpreise)
                self.aktivierungspreis = preisschema['aktivierungspreis']
                self.arbeitspreis = preisschema['arbeitspreis']
                self.bereitstellungspreis = preisschema['bereitstellungspreis']

        # ===============================
        # ===== Datenexport =====
        # ===============================
        def get_preise_dict(self):
            data = dict()
            data['arbeitspreis'] = self.arbeitspreis
            data['bereitstellungspreis'] = self.bereitstellungspreis
            data['aktivierungspreis'] = self.aktivierungspreis
            return data

        def export(self, day):
            data = dict()
            data['name'] = self.name
            data['day'] = day
            data['typ'] = self.typ
            data['nominalleistung'] = self.nominalleistung
            self.sum_gesamtfahrplan()
            data['gesamtfahrplan'] = self.gesamtfahrplan
            data['potential'] = self.potential
            data['arbeitspreis'] = self.arbeitspreis
            data['bereitstellungspreis'] = self.bereitstellungspreis
            data['aktivierungspreis'] = self.aktivierungspreis
            # data['buchungen'] = self.buchungen
            # data['ledger'] = self.ledger
            # data['fahrplaene'] = self.fahrplaene
            data['telemetry'] = self.export_telemetry()
            data['location'] = self.location
            return data  # json.dumps(data, indent=4)

        def export_telemetry(self):
            data = dict()
            data['utilization'] = sum(self.gesamtfahrplan) / sum(self.potential)

            if len(self.tradelength) > 0:
                data['duration'] = sum(self.tradelength) * 0.25 # In stunden!
                data['numtrades'] = len(self.tradelength)
            else:
                data['duration'] = 0
                data['numtrades'] = 0

            einnahmen = dict(arbeitspreis=0.0, bereitstellungspreis=0.0, aktivierungspreis=0.0)
            for key, values in self.ledger.items():
                einnahmen['arbeitspreis'] += values['arbeitspreis']
                einnahmen['bereitstellungspreis'] += values['bereitstellungspreis']
                einnahmen['aktivierungspreis'] += values['aktivierungspreis']
            einnahmen['gesamt'] = einnahmen['arbeitspreis'] + einnahmen['bereitstellungspreis'] + einnahmen['aktivierungspreis']
            data['einnahmen'] = einnahmen
            data['eprofitabilitaet'] = einnahmen['gesamt'] / sum(self.potential)
            return data


# ===== DER SYSTEM Windkraftanlage =====
class Windkraftanlage(DerSystem):
    """ Die ist ein abgleitetetes DER System, welches eine Windkraftanlage (WKA) simuliert
    """

    def __init__(self, name='', typ='WKA', nominalleistung=0.0,
                 preise=dict(arbeitspreis=0.0, bereitstellungspreis=0.0, aktivierungspreis=0.0),
                 position=dict(long=10.00,lat=53.55,timezone=1), strategie=None, locational=None):
        if len(name) == 0: self.name = "DER_WKA_" + str(hash(typ) + 31 * hash(position['long']))
        else: self.name = name

        self.position = position
        super(Windkraftanlage, self).__init__(name=name, typ=typ,
                                              nominalleistung=nominalleistung,
                                              preise=preise, strategie=strategie, locational=locational)

    def erzeuge_tagesprofil(self, tag):
        # Eval der Preise, aber erst ab dem 2. Tag!
        if self.strategie is not None and len(self.angebot) > 0:
            self.eval_prices()

        self.angebot = [0.0 for i in range(96)]

        random.seed(time.time())

        # Schätzung: Alle 2 Stunden ändert sich der Wind zwischen P_Min und P_Max
        # 2 h == 8 Slots, 96 Slots == 12 Chunks
        for i in range(12):
            # Determine Power
            # Lineare Verteilung für Tests
            # power_level = random.random() * self.nominalleistung
            # Weibull-Verteilung mit quasi linearer Leistungsverteilung einer WKA
            power_level = self.__calc_output_power(self.nominalleistung, random.weibullvariate(config.wka_c, config.wka_k))
            for j in range(i*8, (i*8)+8, 1):
                self.angebot[j] = power_level / 4

        # Reset Anlage
        super(Windkraftanlage,self)._reset_anlage()

    def __calc_output_power(self,nominalpower, windspeed):
        """
        Datenapproximation auf Basis einer linearisierten Enercon E82 WKA
        Linearer Bereich von 1m/s bis 14 m/s, Stationärer Bereich von 14 m/s bis 20 m/s, Abschaltung ab v_wind > 20 m/s
        :param nominalpower: Die Nominalleistung der Anlage in MW
        :param windspeed: Windgeschwindigkeit in m/s
        :return: normalized power
        """
        lin_stationary_point = 14.0
        stall_speed = 20.0
        if (windspeed > stall_speed):
            return 0
        elif lin_stationary_point < windspeed < stall_speed:
            return 1
        else:
            return windspeed / lin_stationary_point * nominalpower


# ===== DER SYSTEM SOLARANLAGE =====
class Solaranlage(DerSystem):
    """ Dies ist ein abgleitetetes DER System, welches eine Solaranlage simuliert """

    def __init__(self, name='', typ='PV', nominalleistung=0.0,
                 preise=dict(arbeitspreis=0.0, bereitstellungspreis=0.0, aktivierungspreis=0.0),
                 position=dict(long=10.00,lat=53.55,timezone=1), strategie=None, locational=None):
        if len(name) == 0:
            self.name = "DER_SOL_" + str(hash(typ) + 31 * hash(position['long']))

        self.position = position
        super(Solaranlage, self).__init__(name=name, typ=typ,
                                          nominalleistung=nominalleistung,
                                          preise=preise, strategie=strategie, locational=locational)

    def erzeuge_tagesprofil(self, tag):
        # Eval der Preise, aber erst ab dem 2. Tag!
        if self.strategie is not None and len(self.angebot) > 0:
            self.eval_prices()

        self.angebot = [0.0 for i in range(96)]

        if not config.solar_dynamik: tag = 90

        # Solarberechnungen
        # Sonnenauf und -untergang
        solarzeiten = sun_times(tag, self.position['lat'], self.position['long'], self.position['timezone'])
        q1 = dectime_to_quarter(solarzeiten['sunrise'])                  # In Viertelstunden
        q2 = dectime_to_quarter(solarzeiten['sunset'])                  # In Viertelstunden
        qlen = q2 - q1                                                  # Länge der Sonnenscheindauer

        for i in range(qlen + 1):
            self.angebot[q1+i] = sin((i / qlen) * pi) * self.nominalleistung / 4

        # Reset Anlage
        super(Solaranlage, self)._reset_anlage()

    def export(self, day):
        data = super(Solaranlage,self).export(day)
        data['position'] = self.position
        return data
